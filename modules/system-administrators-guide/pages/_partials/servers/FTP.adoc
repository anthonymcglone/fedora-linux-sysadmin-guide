
[[s1-FTP]]
== FTP
indexterm:[FTP,definition of]indexterm:[FTP,vsftpd]
_File Transfer Protocol_ (`FTP`) is one of the oldest and most commonly used protocols found on the Internet today. Its purpose is to reliably transfer files between computer hosts on a network without requiring the user to log directly into the remote host or have knowledge of how to use the remote system. It allows users to access files on remote systems using a standard set of simple commands.

This section outlines the basics of the `FTP` protocol, as well as configuration options for the primary `FTP` server shipped with {MAJOROS}, [command]#vsftpd#.

[[s2-ftp-protocol]]
=== The File Transfer Protocol
indexterm:[FTP,introducing]
However, because `FTP` is so prevalent on the Internet, it is often required to share files to the public. System administrators, therefore, should be aware of the `FTP` protocol's unique characteristics.

[[s3-ftp-protocol-multiport]]
==== Multiple Ports, Multiple Modes
indexterm:[FTP,command port]indexterm:[FTP,data port]indexterm:[FTP,active mode]indexterm:[FTP,passive mode]
Unlike most protocols used on the Internet, `FTP` requires multiple network ports to work properly. When an `FTP` client application initiates a connection to an `FTP` server, it opens port 21 on the server — known as the _command port_. This port is used to issue all commands to the server. Any data requested from the server is returned to the client via a _data port_. The port number for data connections, and the way in which data connections are initialized, vary depending upon whether the client requests the data in _active_ or _passive_ mode.

The following defines these modes:

active mode::  Active mode is the original method used by the `FTP` protocol for transferring data to the client application. When an active mode data transfer is initiated by the `FTP` client, the server opens a connection from port 20 on the server to the `IP` address and a random, unprivileged port (greater than 1024) specified by the client. This arrangement means that the client machine must be allowed to accept connections over any port above 1024. With the growth of insecure networks, such as the Internet, the use of firewalls to protect client machines is now prevalent. Because these client-side firewalls often deny incoming connections from active mode `FTP` servers, passive mode was devised.

passive mode::  Passive mode, like active mode, is initiated by the `FTP` client application. When requesting data from the server, the `FTP` client indicates it wants to access the data in passive mode and the server provides the `IP` address and a random, unprivileged port (greater than 1024) on the server. The client then connects to that port on the server to download the requested information.
+
While passive mode resolves issues for client-side firewall interference with data connections, it can complicate administration of the server-side firewall. You can reduce the number of open ports on a server by limiting the range of unprivileged ports on the `FTP` server. This also simplifies the process of configuring firewall rules for the server. See xref:File_and_Print_Servers.adoc#s3-ftp-vsftpd-conf-opt-net[Network Options] for more information about limiting passive ports.

[[s2-ftp-servers]]
=== FTP Servers
indexterm:[FTP,server software,vsftpd]indexterm:[FTP,server software,Red Hat Content Accelerator]indexterm:[vsftpd,FTP]indexterm:[vsftpd,security features]
{MAJOROS} ships with two different `FTP` servers:

* [command]#proftpd# - A fast, stable, and highly configurable FTP server.

* [command]#vsftpd# — A fast, secure `FTP` daemon which is the preferred `FTP` server for {MAJOROS}. The remainder of this section focuses on [command]#vsftpd#.

[[s3-ftp-servers-vsftpd]]
==== [command]#vsftpd#

_The Very Secure FTP Daemon_ ([command]#vsftpd#) is designed from the ground up to be fast, stable, and, most importantly, secure. [command]#vsftpd# is the only stand-alone `FTP` server distributed with {MAJOROS}, due to its ability to handle large numbers of connections efficiently and securely.

The security model used by [command]#vsftpd# has three primary aspects:

* *Strong separation of privileged and non-privileged processes* — Separate processes handle different tasks, and each of these processes run with the minimal privileges required for the task.

* *Tasks requiring elevated privileges are handled by processes with the minimal privilege necessary* — By leveraging compatibilities found in the `libcap` library, tasks that usually require full `root` privileges can be executed more safely from a less privileged process.

* *Most processes run in a [command]#chroot# jail* — Whenever possible, processes are change-rooted to the directory being shared; this directory is then considered a [command]#chroot# jail. For example, if the directory [command]#/var/ftp/# is the primary shared directory, [command]#vsftpd# reassigns [command]#/var/ftp/# to the new root directory, known as [command]#/#. This disallows any potential malicious hacker activities for any directories not contained below the new root directory.

Use of these security practices has the following effect on how [command]#vsftpd# deals with requests:

* *The parent process runs with the least privileges required* — The parent process dynamically calculates the level of privileges it requires to minimize the level of risk. Child processes handle direct interaction with the `FTP` clients and run with as close to no privileges as possible.

* *All operations requiring elevated privileges are handled by a small parent process* — Much like the Apache `HTTP` Server, [command]#vsftpd# launches unprivileged child processes to handle incoming connections. This allows the privileged, parent process to be as small as possible and handle relatively few tasks.

* *All requests from unprivileged child processes are distrusted by the parent process* — Communication with child processes are received over a socket, and the validity of any information from child processes is checked before being acted on.

* *Most interaction with `FTP` clients is handled by unprivileged child processes in a [command]#chroot# jail* — Because these child processes are unprivileged and only have access to the directory being shared, any crashed processes only allows the attacker access to the shared files.

[[s3-ftp-vsftpd-conf]]
=== Files Installed with [command]#vsftpd#
indexterm:[vsftpd,RPM,files installed by]
The `vsftpd` RPM installs the daemon (`/usr/sbin/vsftpd`), its configuration and related files, as well as `FTP` directories onto the system. The following lists the files and directories related to [command]#vsftpd# configuration:

* `/etc/rc.d/init.d/vsftpd` — The *initialization script* (_initscript_) used by the [command]#systemctl# command to start, stop, or reload [command]#vsftpd#. See xref:File_and_Print_Servers.adoc#s2-ftp-vsftpd-start[Starting and Stopping [command]#vsftpd#] for more information about using this script.

* `/etc/pam.d/vsftpd` — The Pluggable Authentication Modules (PAM) configuration file for [command]#vsftpd#. This file specifies the requirements a user must meet to login to the `FTP` server. For more information on PAM, refer to the [citetitle]_Using Pluggable Authentication Modules (PAM)_ chapter of the {MAJOROSVER} [citetitle]_Managing Single Sign-On and Smart Cards_ guide.

* `/etc/vsftpd/vsftpd.conf` — The configuration file for [command]#vsftpd#. See xref:File_and_Print_Servers.adoc#s2-ftp-vsftpd-conf[[command]#vsftpd# Configuration Options] for a list of important options contained within this file.

* `/etc/vsftpd/ftpusers` — A list of users not allowed to log into [command]#vsftpd#. By default, this list includes the `root`, `bin`, and `daemon` users, among others.

* `/etc/vsftpd/user_list` — This file can be configured to either deny or allow access to the users listed, depending on whether the [command]#userlist_deny# directive is set to [command]#YES# (default) or [command]#NO# in `/etc/vsftpd/vsftpd.conf`. If `/etc/vsftpd/user_list` is used to grant access to users, the usernames listed must *not* appear in `/etc/vsftpd/ftpusers`.

* `/var/ftp/` — The directory containing files served by [command]#vsftpd#. It also contains the `/var/ftp/pub/` directory for anonymous users. Both directories are world-readable, but writable only by the `root` user.

[[s2-ftp-vsftpd-start]]
=== Starting and Stopping [command]#vsftpd#
indexterm:[vsftpd,starting]indexterm:[vsftpd,stopping]indexterm:[vsftpd,status]indexterm:[vsftpd,condrestart]indexterm:[vsftpd,restarting]
The `vsftpd` RPM installs the `/etc/rc.d/init.d/vsftpd` script, which can be accessed using the [command]#systemctl# command.

To start the server, as `root` type:

[subs="quotes, macros"]
----
[command]#systemctl start vsftpd.service#
----

To stop the server, as `root` type:

[subs="quotes, macros"]
----
[command]#systemctl stop vsftpd.service#
----

The [option]`restart` option is a shorthand way of stopping and then starting [command]#vsftpd#. This is the most efficient way to make configuration changes take effect after editing the configuration file for [command]#vsftpd#.

To restart the server, as `root` type:

[subs="quotes, macros"]
----
[command]#systemctl restart vsftpd.service#
----

The [option]`condrestart` (_conditional restart_) option only starts [command]#vsftpd# if it is currently running. This option is useful for scripts, because it does not start the daemon if it is not running.

To conditionally restart the server, as `root` type:

[subs="quotes, macros"]
----
[command]#systemctl condrestart vsftpd.service#
----

By default, the [command]#vsftpd# service does *not* start automatically at boot time. To configure the [command]#vsftpd# service to start at boot time, use a service manager such as [command]#systemctl#. See xref:infrastructure-services/Services_and_Daemons.adoc#ch-Services_and_Daemons[Services and Daemons] for more information on how to configure services in {MAJOROS}.

[[s3-ftp-firewalld]]
==== Configuring the Firewall for FTP
By default, `firewalld` blocks incoming FTP connections. To allow FTP connections, as `root` type:

[subs="quotes, macros"]
----
[command]#firewall-cmd --add-service=ftp#
----

The change will be applied immediately, but will be lost next time `firewalld` is reloaded or the system restarted. To make it permanent, type:

[subs="quotes, macros"]
----
[command]#firewall-cmd --permanent --add-service=ftp#
----

For more information on configuring `firewalld`, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide].

[[s3-ftp-vsftpd-start-multi]]
==== Starting Multiple Copies of [command]#vsftpd#
indexterm:[vsftpd,starting multiple copies of]indexterm:[vsftpd,multihome configuration]
Sometimes one computer is used to serve multiple `FTP` domains. This is a technique called _multihoming_. One way to multihome using [command]#vsftpd# is by running multiple copies of the daemon, each with its own configuration file.

To do this, first assign all relevant `IP` addresses to network devices or alias network devices on the system. For more information about configuring network devices, device aliases, and additional information about network configuration scripts, refer to the [citetitle]_link:++https://docs.fedoraproject.org/en-US/Fedora/networking++[{MAJOROS} Networking Guide]_.

Next, the DNS server for the `FTP` domains must be configured to reference the correct machine. For information about BIND and its configuration files, refer to the [citetitle]_link:++https://docs.fedoraproject.org/en-US/Fedora/networking++[{MAJOROS} Networking Guide]_.

If there is more configuration files present in the `/etc/vsftpd` directory, calling [command]#systemctl start vsftpd.service# results in the `/etc/rc.d/init.d/vsftpd` initscript starting the same number of processes as the number of configuration files. Each configuration file must have a unique name in the `/etc/vsftpd/` directory and must be readable and writable only by `root`.

[[s2-ftp-vsftpd-conf]]
===== [command]#vsftpd# Configuration Options
indexterm:[vsftpd,configuration file,format of]indexterm:[vsftpd,configuration file,/etc/vsftpd/vsftpd.conf]
Although [command]#vsftpd# may not offer the level of customization other widely available `FTP` servers have, it offers enough options to fill most administrator's needs. The fact that it is not overly feature-laden limits configuration and programmatic errors.

All configuration of [command]#vsftpd# is handled by its configuration file, `/etc/vsftpd/vsftpd.conf`. Each directive is on its own line within the file and follows the following format:

[subs="quotes, macros"]
----
_directive_pass:attributes[{blank}]=pass:attributes[{blank}]_value_
----

For each directive, replace _directive_ with a valid directive and _value_ with a valid value.

.Do not use spaces
[IMPORTANT]
====

There must not be any spaces between the _directive_, equal symbol, and the _value_ in a directive.

====

Comment lines must be preceded by a hash sign ([command]###) and are ignored by the daemon.

For a complete list of all directives available, refer to the man page for `vsftpd.conf`.

.Securing the vsftpd service
[IMPORTANT]
====

For an overview of ways to secure [command]#vsftpd#, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide].

====

The following is a list of some of the more important directives within `/etc/vsftpd/vsftpd.conf`. All directives not explicitly found or commented out within [command]#vsftpd#pass:attributes[{blank}]'s configuration file are set to their default value.

[[s3-ftp-vsftpd-conf-opt-daemon]]
==== Daemon Options
indexterm:[vsftpd,configuration file,daemon options]
The following is a list of directives which control the overall behavior of the [command]#vsftpd# daemon.

* [command]#listen# — When enabled, [command]#vsftpd# runs in stand-alone mode. {MAJOROS} sets this value to [command]#YES#. This directive cannot be used in conjunction with the [command]#listen_ipv6# directive.
+
The default value is [command]#NO#.

* [command]#listen_ipv6# — When enabled, [command]#vsftpd# runs in stand-alone mode, but listens only to `IPv6` sockets. This directive cannot be used in conjunction with the [command]#listen# directive.
+
The default value is [command]#NO#.

* [command]#session_support# — When enabled, [command]#vsftpd# attempts to maintain login sessions for each user through Pluggable Authentication Modules (PAM). For more information, refer to the [citetitle]_Using Pluggable Authentication Modules (PAM)_ chapter of the Red Hat Enterprise Linux 6 [citetitle]_Managing Single Sign-On and Smart Cards_ and the PAM man pages. . If session logging is not necessary, disabling this option allows [command]#vsftpd# to run with less processes and lower privileges.
+
The default value is [command]#YES#.

[[s3-ftp-vsftpd-conf-opt-login]]
==== Log In Options and Access Controls
indexterm:[vsftpd,configuration file,login options]indexterm:[vsftpd,configuration file,access controls]
The following is a list of directives which control the login behavior and access control mechanisms.

* [command]#anonymous_enable# — When enabled, anonymous users are allowed to log in. The usernames `anonymous` and `ftp` are accepted.
+
The default value is [command]#YES#.
+
See xref:File_and_Print_Servers.adoc#s3-ftp-vsftpd-conf-opt-anon[Anonymous User Options] for a list of directives affecting anonymous users.

* [command]#banned_email_file# — If the [command]#deny_email_enable# directive is set to [command]#YES#, this directive specifies the file containing a list of anonymous email passwords which are not permitted access to the server.
+
The default value is `/etc/vsftpd/banned_emails`.

* [command]#banner_file# — Specifies the file containing text displayed when a connection is established to the server. This option overrides any text specified in the [command]#ftpd_banner# directive.
+
There is no default value for this directive.

* [command]#cmds_allowed# — Specifies a comma-delimited list of `FTP` commands allowed by the server. All other commands are rejected.
+
There is no default value for this directive.

* [command]#deny_email_enable# — When enabled, any anonymous user utilizing email passwords specified in the `/etc/vsftpd/banned_emails` are denied access to the server. The name of the file referenced by this directive can be specified using the [command]#banned_email_file# directive.
+
The default value is [command]#NO#.

* [command]#ftpd_banner# — When enabled, the string specified within this directive is displayed when a connection is established to the server. This option can be overridden by the [command]#banner_file# directive.
+
By default [command]#vsftpd# displays its standard banner.

* [command]#local_enable# — When enabled, local users are allowed to log into the system.
+
The default value is [command]#YES#.
+
See xref:File_and_Print_Servers.adoc#s3-ftp-vsftpd-conf-opt-usr[Local User Options] for a list of directives affecting local users.

* [command]#pam_service_name# — Specifies the PAM service name for [command]#vsftpd#.
+
The default value is [command]#ftp#. Note, in {MAJOROS}, the value is set to [command]#vsftpd#.

* The default value is [command]#NO#. Note, in {MAJOROS}, the value is set to [command]#YES#.

* [command]#userlist_deny# — When used in conjunction with the [command]#userlist_enable# directive and set to [command]#NO#, all local users are denied access unless the username is listed in the file specified by the [command]#userlist_file# directive. Because access is denied before the client is asked for a password, setting this directive to [command]#NO# prevents local users from submitting unencrypted passwords over the network.
+
The default value is [command]#YES#.

* [command]#userlist_enable# — When enabled, the users listed in the file specified by the [command]#userlist_file# directive are denied access. Because access is denied before the client is asked for a password, users are prevented from submitting unencrypted passwords over the network.
+
The default value is [command]#NO#, however under {MAJOROS} the value is set to [command]#YES#.

* [command]#userlist_file# — Specifies the file referenced by [command]#vsftpd# when the [command]#userlist_enable# directive is enabled.
+
The default value is [command]#/etc/vsftpd/user_list# and is created during installation.

[[s3-ftp-vsftpd-conf-opt-anon]]
==== Anonymous User Options
indexterm:[vsftpd,configuration file,anonymous user options]
The following lists directives which control anonymous user access to the server. To use these options, the [command]#anonymous_enable# directive must be set to [command]#YES#.

* [command]#anon_mkdir_write_enable# — When enabled in conjunction with the [command]#write_enable# directive, anonymous users are allowed to create new directories within a parent directory which has write permissions.
+
The default value is [command]#NO#.

* [command]#anon_root# — Specifies the directory [command]#vsftpd# changes to after an anonymous user logs in.
+
There is no default value for this directive.

* [command]#anon_upload_enable# — When enabled in conjunction with the [command]#write_enable# directive, anonymous users are allowed to upload files within a parent directory which has write permissions.
+
The default value is [command]#NO#.

* [command]#anon_world_readable_only# — When enabled, anonymous users are only allowed to download world-readable files.
+
The default value is [command]#YES#.

* [command]#ftp_username# — Specifies the local user account (listed in `/etc/passwd`) used for the anonymous `FTP` user. The home directory specified in `/etc/passwd` for the user is the root directory of the anonymous `FTP` user.
+
The default value is [command]#ftp#.

* [command]#no_anon_password# — When enabled, the anonymous user is not asked for a password.
+
The default value is [command]#NO#.

* [command]#secure_email_list_enable# — When enabled, only a specified list of email passwords for anonymous logins are accepted. This is a convenient way to offer limited security to public content without the need for virtual users.
+
Anonymous logins are prevented unless the password provided is listed in [command]#/etc/vsftpd/email_passwords#. The file format is one password per line, with no trailing white spaces.
+
The default value is [command]#NO#.

[[s3-ftp-vsftpd-conf-opt-usr]]
==== Local User Options
indexterm:[vsftpd,configuration file,local user options]
The following lists directives which characterize the way local users access the server. To use these options, the [command]#local_enable# directive must be set to [command]#YES#.

* [command]#chmod_enable# — When enabled, the `FTP` command [command]#SITE CHMOD# is allowed for local users. This command allows the users to change the permissions on files.
+
The default value is [command]#YES#.

* [command]#chroot_list_enable# — When enabled, the local users listed in the file specified in the [command]#chroot_list_file# directive are placed in a [command]#chroot# jail upon log in.
+
If enabled in conjunction with the [command]#chroot_local_user# directive, the local users listed in the file specified in the [command]#chroot_list_file# directive are *not* placed in a [command]#chroot# jail upon log in.
+
The default value is [command]#NO#.

* [command]#chroot_list_file# — Specifies the file containing a list of local users referenced when the [command]#chroot_list_enable# directive is set to [command]#YES#.
+
The default value is [command]#/etc/vsftpd/chroot_list#.

* [command]#chroot_local_user# — When enabled, local users are change-rooted to their home directories after logging in.
+
The default value is [command]#NO#.
+
.Avoid enabling the chroot_local_user option
[WARNING]
====

Enabling [command]#chroot_local_user# opens up a number of security issues, especially for users with upload privileges. For this reason, it is *not* recommended.

====

* [command]#guest_enable# — When enabled, all non-anonymous users are logged in as the user [command]#guest#, which is the local user specified in the [command]#guest_username# directive.
+
The default value is [command]#NO#.

* [command]#guest_username# — Specifies the username the [command]#guest# user is mapped to.
+
The default value is [command]#ftp#.

* [command]#local_root# — Specifies the directory [command]#vsftpd# changes to after a local user logs in.
+
There is no default value for this directive.

* [command]#local_umask# — Specifies the umask value for file creation. Note that the default value is in octal form (a numerical system with a base of eight), which includes a "`0`" prefix. Otherwise the value is treated as a base-10 integer.
+
The default value is [command]#022#.

* [command]#passwd_chroot_enable# — When enabled in conjunction with the [command]#chroot_local_user# directive, [command]#vsftpd# change-roots local users based on the occurrence of the [command]#/./# in the home directory field within `/etc/passwd`.
+
The default value is [command]#NO#.

* [command]#user_config_dir# — Specifies the path to a directory containing configuration files bearing the name of local system users that contain specific setting for that user. Any directive in the user's configuration file overrides those found in `/etc/vsftpd/vsftpd.conf`.
+
There is no default value for this directive.

[[s3-ftp-vsftpd-conf-opt-dir]]
==== Directory Options
indexterm:[vsftpd,configuration file,directory options]
The following lists directives which affect directories.

* [command]#dirlist_enable# — When enabled, users are allowed to view directory lists.
+
The default value is [command]#YES#.

* [command]#dirmessage_enable# — When enabled, a message is displayed whenever a user enters a directory with a message file. This message resides within the current directory. The name of this file is specified in the [command]#message_file# directive and is `.message` by default.
+
The default value is [command]#NO#. Note, in {MAJOROS}, the value is set to [command]#YES#.

* [command]#force_dot_files# — When enabled, files beginning with a dot (`.`) are listed in directory listings, with the exception of the `.` and `..` files.
+
The default value is [command]#NO#.

* [command]#hide_ids# — When enabled, all directory listings show `ftp` as the user and group for each file.
+
The default value is [command]#NO#.

* [command]#message_file# — Specifies the name of the message file when using the [command]#dirmessage_enable# directive.
+
The default value is [command]#.message#.

* [command]#text_userdb_names# — When enabled, text usernames and group names are used in place of UID and GID entries. Enabling this option may slow performance of the server.
+
The default value is [command]#NO#.

* [command]#use_localtime# — When enabled, directory listings reveal the local time for the computer instead of GMT.
+
The default value is [command]#NO#.

[[s3-ftp-vsftpd-conf-opt-file]]
==== File Transfer Options
indexterm:[vsftpd,configuration file,file transfer options]
The following lists directives which affect directories.

* [command]#download_enable# — When enabled, file downloads are permitted.
+
The default value is [command]#YES#.

* [command]#chown_uploads# — When enabled, all files uploaded by anonymous users are owned by the user specified in the [command]#chown_username# directive.
+
The default value is [command]#NO#.

* [command]#chown_username# — Specifies the ownership of anonymously uploaded files if the [command]#chown_uploads# directive is enabled.
+
The default value is [command]#root#.

* [command]#write_enable# — When enabled, `FTP` commands which can change the file system are allowed, such as [command]#DELE#, [command]#RNFR#, and [command]#STOR#.
+
The default value is [command]#YES#.

[[s3-ftp-vsftpd-conf-opt-log]]
==== Logging Options
indexterm:[vsftpd,configuration file,logging options]
The following lists directives which affect [command]#vsftpd#pass:attributes[{blank}]'s logging behavior.

* [command]#dual_log_enable# — When enabled in conjunction with [command]#xferlog_enable#, [command]#vsftpd# writes two files simultaneously: a [command]#wu-ftpd#-compatible log to the file specified in the [command]#xferlog_file# directive (`/var/log/xferlog` by default) and a standard [command]#vsftpd# log file specified in the [command]#vsftpd_log_file# directive (`/var/log/vsftpd.log` by default).
+
The default value is [command]#NO#.

* [command]#log_ftp_protocol# — When enabled in conjunction with [command]#xferlog_enable# and with [command]#xferlog_std_format# set to [command]#NO#, all `FTP` commands and responses are logged. This directive is useful for debugging.
+
The default value is [command]#NO#.

* [command]#syslog_enable# — When enabled in conjunction with [command]#xferlog_enable#, all logging normally written to the standard [command]#vsftpd# log file specified in the [command]#vsftpd_log_file# directive (`/var/log/vsftpd.log` by default) is sent to the system logger instead under the `FTPD` facility.
+
The default value is [command]#NO#.

* [command]#vsftpd_log_file# — Specifies the [command]#vsftpd# log file. For this file to be used, [command]#xferlog_enable# must be enabled and [command]#xferlog_std_format# must either be set to [command]#NO# or, if [command]#xferlog_std_format# is set to [command]#YES#, [command]#dual_log_enable# must be enabled. It is important to note that if [command]#syslog_enable# is set to [command]#YES#, the system log is used instead of the file specified in this directive.
+
The default value is `/var/log/vsftpd.log`.

* [command]#xferlog_enable# — When enabled, [command]#vsftpd# logs connections ([command]#vsftpd# format only) and file transfer information to the log file specified in the [command]#vsftpd_log_file# directive (`/var/log/vsftpd.log` by default). If [command]#xferlog_std_format# is set to [command]#YES#, file transfer information is logged but connections are not, and the log file specified in [command]#xferlog_file# (`/var/log/xferlog` by default) is used instead. It is important to note that both log files and log formats are used if [command]#dual_log_enable# is set to [command]#YES#.
+
The default value is [command]#NO#. Note, in {MAJOROS}, the value is set to [command]#YES#.

* [command]#xferlog_file# — Specifies the [command]#wu-ftpd#-compatible log file. For this file to be used, [command]#xferlog_enable# must be enabled and [command]#xferlog_std_format# must be set to [command]#YES#. It is also used if [command]#dual_log_enable# is set to [command]#YES#.
+
The default value is `/var/log/xferlog`.

* [command]#xferlog_std_format# — When enabled in conjunction with [command]#xferlog_enable#, only a [command]#wu-ftpd#-compatible file transfer log is written to the file specified in the [command]#xferlog_file# directive (`/var/log/xferlog` by default). It is important to note that this file only logs file transfers and does not log connections to the server.
+
The default value is [command]#NO#. Note, in {MAJOROS}, the value is set to [command]#YES#.

.Maintaining compatibility with older log file formats
[IMPORTANT]
====

To maintain compatibility with log files written by the older [command]#wu-ftpd# `FTP` server, the [command]#xferlog_std_format# directive is set to [command]#YES# under {MAJOROS}. However, this setting means that connections to the server are not logged.

To both log connections in [command]#vsftpd# format and maintain a [command]#wu-ftpd#-compatible file transfer log, set [command]#dual_log_enable# to [command]#YES#.

If maintaining a [command]#wu-ftpd#-compatible file transfer log is not important, either set [command]#xferlog_std_format# to [command]#NO#, comment the line with a hash sign ([command]###), or delete the line entirely.

====

[[s3-ftp-vsftpd-conf-opt-net]]
==== Network Options
indexterm:[vsftpd,configuration file,network options]
The following lists directives which affect how [command]#vsftpd# interacts with the network.

* [command]#accept_timeout# — Specifies the amount of time for a client using passive mode to establish a connection.
+
The default value is [command]#60#.

* [command]#anon_max_rate# — Specifies the maximum data transfer rate for anonymous users in bytes per second.
+
The default value is [command]#0#, which does not limit the transfer rate.

* [command]#connect_from_port_20# When enabled, [command]#vsftpd# runs with enough privileges to open port 20 on the server during active mode data transfers. Disabling this option allows [command]#vsftpd# to run with less privileges, but may be incompatible with some `FTP` clients.
+
The default value is [command]#NO#. Note, in {MAJOROS}, the value is set to [command]#YES#.

* [command]#connect_timeout# — Specifies the maximum amount of time a client using active mode has to respond to a data connection, in seconds.
+
The default value is [command]#60#.

* [command]#data_connection_timeout# — Specifies maximum amount of time data transfers are allowed to stall, in seconds. Once triggered, the connection to the remote client is closed.
+
The default value is [command]#300#.

* [command]#ftp_data_port# — Specifies the port used for active data connections when [command]#connect_from_port_20# is set to [command]#YES#.
+
The default value is [command]#20#.

* [command]#idle_session_timeout# — Specifies the maximum amount of time between commands from a remote client. Once triggered, the connection to the remote client is closed.
+
The default value is [command]#300#.

* [command]#listen_address# — Specifies the `IP` address on which [command]#vsftpd# listens for network connections.
+
There is no default value for this directive.
+
.Running multiple copies of vsftpd
[NOTE]
====

If running multiple copies of [command]#vsftpd# serving different `IP` addresses, the configuration file for each copy of the [command]#vsftpd# daemon must have a different value for this directive. See xref:File_and_Print_Servers.adoc#s3-ftp-vsftpd-start-multi[Starting Multiple Copies of [command]#vsftpd#] for more information about multihomed `FTP` servers.

====

* [command]#listen_address6# — Specifies the `IPv6` address on which [command]#vsftpd# listens for network connections when [command]#listen_ipv6# is set to [command]#YES#.
+
There is no default value for this directive.
+
.Running multiple copies of vsftpd
[NOTE]
====

If running multiple copies of [command]#vsftpd# serving different `IP` addresses, the configuration file for each copy of the [command]#vsftpd# daemon must have a different value for this directive. See xref:File_and_Print_Servers.adoc#s3-ftp-vsftpd-start-multi[Starting Multiple Copies of [command]#vsftpd#] for more information about multihomed `FTP` servers.

====

* [command]#listen_port# — Specifies the port on which [command]#vsftpd# listens for network connections.
+
The default value is [command]#21#.

* [command]#local_max_rate# — Specifies the maximum rate data is transferred for local users logged into the server in bytes per second.
+
The default value is [command]#0#, which does not limit the transfer rate.

* [command]#max_clients# — Specifies the maximum number of simultaneous clients allowed to connect to the server when it is running in standalone mode. Any additional client connections would result in an error message.
+
The default value is [command]#0#, which does not limit connections.

* [command]#max_per_ip# — Specifies the maximum of clients allowed to connected from the same source `IP` address.
+
The default value is [command]#0#, which does not limit connections.

* [command]#pasv_address# — Specifies the `IP` address for the public facing `IP` address of the server for servers behind Network Address Translation (NAT) firewalls. This enables [command]#vsftpd# to hand out the correct return address for passive mode connections.
+
There is no default value for this directive.

* [command]#pasv_enable# — When enabled, passive mode connects are allowed.
+
The default value is [command]#YES#.

* [command]#pasv_max_port# — Specifies the highest possible port sent to the `FTP` clients for passive mode connections. This setting is used to limit the port range so that firewall rules are easier to create.
+
The default value is [command]#0#, which does not limit the highest passive port range. The value must not exceed [command]#65535#.

* [command]#pasv_min_port# — Specifies the lowest possible port sent to the `FTP` clients for passive mode connections. This setting is used to limit the port range so that firewall rules are easier to create.
+
The default value is [command]#0#, which does not limit the lowest passive port range. The value must not be lower [command]#1024#.

* [command]#pasv_promiscuous# — When enabled, data connections are not checked to make sure they are originating from the same `IP` address. This setting is only useful for certain types of tunneling.
+
.Avoid enabling the pasv_promiscuous option
[WARNING]
====

Do not enable this option unless absolutely necessary as it disables an important security feature which verifies that passive mode connections originate from the same `IP` address as the control connection that initiates the data transfer.

====
+
The default value is [command]#NO#.

* [command]#port_enable# — When enabled, active mode connects are allowed.
+
The default value is [command]#YES#.

[[s2-ftp-resources]]
=== Additional Resources
indexterm:[vsftpd,additional resources]
For more information about [command]#vsftpd#, refer to the following resources.

[[s3-ftp-installed-documentation]]
==== Installed Documentation
indexterm:[vsftpd,additional resources,installed documentation]

* The `/usr/share/doc/vsftpd/` directory — This directory contains a `README` with basic information about the software. The `TUNING` file contains basic performance tuning tips and the `SECURITY/` directory contains information about the security model employed by [command]#vsftpd#.

* [command]#vsftpd# related man pages — There are a number of man pages for the daemon and configuration files. The following lists some of the more important man pages.
+
Server Applications::  {blank}
+
*** [command]#man vsftpd# — Describes available command line options for [command]#vsftpd#.
+
Configuration Files::  {blank}
+
*** [command]#man vsftpd.conf# — Contains a detailed list of options available within the configuration file for [command]#vsftpd#.
+
*** [command]#man 5 hosts_access#  — Describes the format and options available within the TCP wrappers configuration files: `hosts.allow` and `hosts.deny`.

[[s3-ftp-useful-websites]]
==== Useful Websites
indexterm:[vsftpd,additional resources,useful websites]

* link:++https://security.appspot.com/vsftpd.html++[https://security.appspot.com/vsftpd.html] — The [command]#vsftpd# project page is a great place to locate the latest documentation and to contact the author of the software.

* link:++https://slacksite.com/other/ftp.html++[https://slacksite.com/other/ftp.html] — This website provides a concise explanation of the differences between active and passive mode `FTP`.

* link:++https://www.ietf.org/rfc/rfc0959.txt++[https://www.ietf.org/rfc/rfc0959.txt] — The original _Request for Comments_ (_RFC_) of the `FTP` protocol from the IETF.
